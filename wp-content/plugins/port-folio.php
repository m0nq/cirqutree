<?php
/**
 * Plugin Name: Port.Folio
 * Plugin URI:
 * Description: This plugin displays a portfolio
 * Author: Monk Wellington
 * Author URI: http://m0nq.github.io/
 * License: GPLv2+
 * License: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: port-folio
 * Domain Path:
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * Register Post Type
 */
function pp_register_post_type() {
	$labels = [
		'name'           => _x( 'Portfolio', 'Post type general name', 'port-folio' ),
		'singular_name'  => _x( 'Portfolio', 'Post type singular name', 'port-folio' ),
		'menu_name'      => _x( 'Portfolio', 'Admin Menu text', 'port-folio' ),
		'name_admin_bar' => _x( 'Portfolio Items', 'Add New on Toolbar', 'port-folio' )
	];
	$args   = [
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => [ 'slug' => 'portfolio' ],
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => [ 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ],
		'menu_icon'          => 'dashicons-screenoptions'
	];
	register_post_type( 'pp_portfolio', $args );
}

/*
 * Register Taxonomy
 */
function pp_create_taxonomy() {
	$labels = [
		'name'          => _x( 'Item Type', 'taxonomy general name', 'port-folio' ),
		'singuler_name' => _x( 'Item Type', 'taxonomy singuler name', 'port-folio' )
	];
	$args   = [
		'labels'           => $labels,
		'show_ui'          => true,
		'rewrite'          => [ 'slug' => 'item-type' ],
		'hierarchical'     => true,
		'show_admin_colum' => true,
		'query_var'        => true
	];
	register_taxonomy( 'pp_item_type', 'pp_portfolio', $args );
}

add_action( 'init', 'pp_register_post_type' );
add_action( 'init', 'pp_create_taxonomy' );
